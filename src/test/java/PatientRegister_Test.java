import edu.ntnu.IDATT2001.brigittb.Patient.Model.Patient;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.PatientRegister;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegister_Test {
    @Test
    public void checkIfClassIsInstantiable_test() throws IllegalStateException {
        assertThrows(IllegalStateException.class, PatientRegister::new);
    }

    @Test
    public void checkIfPatientIsAddedToList_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);
        assertEquals(1, PatientRegister.getPatients_register().size());
    }

    @Test
    public void checkIfPatientIsRemovedFromList_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);

        PatientRegister.deletePatientFromRegister(patient);
        Assertions.assertEquals(0, PatientRegister.getPatients_register().size());
    }

    @Test
    public void checkIfPatientIsEdited_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);

        PatientRegister.editPatientInRegister(patient, "Ole", "Brum", "NasseNøff", "222222t", "honning mangel");
        assertNotEquals(patient, PatientRegister.getPatients_register().get(0));
    }

    @Test
    public void checkIfExceptionIsThrownWhenAddingExistingPatient_ShouldThrowException_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);
        assertThrows(Exception.class, () -> PatientRegister.addPatientToRegister(patient));
    }
    @Test
    public void checkIfExceptionIsThrownWhenDeletingPatient_ShouldNotThrowException_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);
        assertDoesNotThrow(() -> PatientRegister.deletePatientFromRegister(patient));
    }

    @Test
    public void checkIfExceptionIsThrownWhenNonExistingDeletingPatient_ShouldThrowException_test() throws Exception {
        PatientRegister.getPatients_register().clear();
        Patient patient = new Patient("000000", "Ole", "Olsen", "222222t", "");
        PatientRegister.addPatientToRegister(patient);

        assertThrows(Exception.class, () -> PatientRegister.deletePatientFromRegister(new Patient("Helly","Hansen","Ole Olsen","000000","non")));
    }

}
