import edu.ntnu.IDATT2001.brigittb.Patient.Model.FileHandler;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.PatientRegister;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

public class fileHandler_test {
@Test
    public void checkIfImportWorks() throws Exception{
    FileHandler.importCSV("src/test/resources/testData.csv");
    assertEquals("1234567", PatientRegister.getPatients_register().get(0).getSocialSecurityNumber());
}

@Test
    public void checkIfExportWorks() throws Exception{
    FileHandler.importCSV("src/test/resources/testData.csv");
    FileHandler.exportCSV("src/test/resources/testData2.csv");
    assertTrue(Files.deleteIfExists(Path.of("src/test/resources/testData2.csv")));
}

}
