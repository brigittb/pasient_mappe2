module Pasient.Mappe2 {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.logging;

    exports edu.ntnu.IDATT2001.brigittb.Patient.Model to java.base;
    opens edu.ntnu.IDATT2001.brigittb.Patient.Model;
    exports edu.ntnu.IDATT2001.brigittb.Patient.Controllers;
    opens edu.ntnu.IDATT2001.brigittb.Patient.Controllers to javafx.fxml;
    exports edu.ntnu.IDATT2001.brigittb.Patient.App;
    opens edu.ntnu.IDATT2001.brigittb.Patient.App to javafx.fxml;
}