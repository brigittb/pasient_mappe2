package edu.ntnu.IDATT2001.brigittb.Patient.Model;


/**
 * A singleton -class used to select patients from the tableview.
 * Manages each selected patient from the list as an single Instance of this class.
 */
public class Selector {


    private Patient patient;
    private final static Selector INSTANCE = new Selector();

    private Selector() {}

    public static Selector getInstance() {
        return INSTANCE;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Patient getPatient() {
        return this.patient;
    }

}
