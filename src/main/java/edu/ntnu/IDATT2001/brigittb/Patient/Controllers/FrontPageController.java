package edu.ntnu.IDATT2001.brigittb.Patient.Controllers;

import edu.ntnu.IDATT2001.brigittb.Patient.Model.FileHandler;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.Patient;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.PatientRegister;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.Selector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class FrontPageController implements Initializable {


  @FXML private Label statusBar;
  @FXML private TableView<Patient> patientsTableView;
  @FXML private TableColumn<Patient,String> firstname_column, lastname_column, sos_sec_number_column, diagnosis_column, gp_column ;

  private static ObservableList<Patient> patientList = FXCollections.observableArrayList();


  /**
   * Updates the patientList in tableview on the frontpage
   */
  public static void update(){
    patientList.clear();
    patientList.addAll(PatientRegister.getPatients_register());
  }

  /**
   * Uses the Selector to chose a single Patient
   * @throws Exception when nothing is selected
   */

  public void getSelectedItem() throws Exception {
    Selector selector = Selector.getInstance();

    if (patientsTableView.getSelectionModel().getSelectedItem() == null) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setTitle("Error");
      alert.setHeaderText("No patient selected");
      alert.show();

      throw new Exception("Non selected");
    }
    selector.setPatient(patientsTableView.getSelectionModel().getSelectedItem());

  }

  /**
   * Updates the user with status messages
   * @param currentStatus of the system
   */
  public void setStatusBar(String currentStatus){
    statusBar.setText(currentStatus);
  }


  /**
   * Opens a new window/stage to edit the selected Patient
   * @throws Exception if nothing is selected when clicked
   */
  public void editPatient() throws Exception {
    getSelectedItem();
    Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/edu/ntnu/IDATT2001/brigittb/Patient/FXML/edit_patient.fxml")));
    Stage stage = new Stage();
    stage.setTitle("Edit new patient");
    stage.setScene(new Scene(root));
    stage.show();

  }

  /**
   * Opens a new window/stage to add a new Patient
   * @throws Exception if the added patient already exists
   */
  public void addPatient() throws Exception {

    Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/edu/ntnu/IDATT2001/brigittb/Patient/FXML/add_patient.fxml")));
    Stage stage = new Stage();
    stage.setTitle("Edit new patient");
    stage.setScene(new Scene(root));
    stage.show();
  }

  /**
   * Removes patient from register
   * @throws Exception if the chosen patient does not exist
   */
  public void removePatient() throws Exception {
    getSelectedItem();

    try{
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      alert.setTitle("Delete patient");
      alert.setHeaderText("Delete selected");
      alert.setContentText("Are you sure you want to delete this patient from the list?");
      alert.showAndWait();

      Optional<ButtonType> res = alert.showAndWait();
      ButtonType button = res.orElse(ButtonType.CANCEL);

      if (button == ButtonType.OK) {
        Patient selectedPatientToEdit = Selector.getInstance().getPatient();
        PatientRegister.deletePatientFromRegister(selectedPatientToEdit);
        update();
        statusBar.setText("Successfully deleted");
      }
      patientsTableView.refresh();
    }catch (Exception e){
      statusBar.setText("Deletion failed");
      e.printStackTrace();
    }

  }


  /**
   * Imports .csv files into the system. Only supports files that end with .csv - Alert triggers if not
   * Updates the status bar on execution
   */
  public void setImportFile() {

    try {
      FileChooser chooseFile = new FileChooser();
      chooseFile.setTitle("Select csv file for import");
      File file = chooseFile.showOpenDialog(patientsTableView.getScene().getWindow());

      if (file==null) {
        return;
      }

      String fileName = file.getName();

      String format = "";
      if (fileName.lastIndexOf(".") > 0) format = fileName.substring(fileName.lastIndexOf(".")+1);


      if(!format.equals("csv")) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Select file error");
        alert.setHeaderText("Selected file format is not supported");
        alert.setContentText("Choose another file with .csv format");
        alert.showAndWait();

      }else {
        FileHandler.importCSV(file.getAbsolutePath());
        patientsTableView.refresh();
        setStatusBar("Import succeeded");
      }

    }catch (Exception e){
      patientsTableView.refresh();
      setStatusBar("Import failed");
      e.printStackTrace();
    }
  }


  /**
   * Exports .csv files into the filesystem.  Alert triggers if
   * a file with the name already exists
   * Updates the status bar on execution
   */
  public void setExportFile() {
    try {
      FileChooser chosenFile = new FileChooser();
      chosenFile.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV-file (.csv)", ".csv"));
      File save = chosenFile.showSaveDialog(patientsTableView.getScene().getWindow());

      //Alert
      if (!save.createNewFile() && patientList.isEmpty()) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Export file error");
        alert.setHeaderText("This file already exists.");
        alert.setContentText("Do you want to overwrite it?");

        Optional<ButtonType> res = alert.showAndWait();
        if (res.isPresent() && res.get() != ButtonType.OK) {
          return;
        }else{
          FileHandler.exportCSV(save.getPath());
          setStatusBar("Export succeeded");
        }
      }

    } catch (Exception e) {
      setStatusBar("Export failed");
    }
  }

  public void menuBarExit()  {
    Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
    exitAlert.setTitle("Exit");
    exitAlert.setHeaderText("Exit system");
    exitAlert.setContentText("Are you sure you want to exit?");

    Optional<ButtonType> res = exitAlert.showAndWait();
    ButtonType button = res.orElse(ButtonType.CANCEL);

    if (button == ButtonType.OK) {
      System.exit(0);

    } else {
      System.out.println("canceled");
    }
  }
  public void menuBarAbout() {
    Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
    infoAlert.setTitle("About");
    infoAlert.setHeaderText("Patients_Mappe2\nv0.1-SNAPSHOT");
    infoAlert.setContentText("A patients registration application made by\n© Brigitt Bright\n2021-05-05");
    infoAlert.show();
  }


  @Override
  public void initialize(URL location, ResourceBundle resources) {
    update();
    firstname_column.setCellValueFactory(new PropertyValueFactory<>("firstName"));
    lastname_column.setCellValueFactory(new PropertyValueFactory<>("lastName"));
    sos_sec_number_column.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
    gp_column.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    diagnosis_column.setCellValueFactory(new PropertyValueFactory<>("Diagnosis"));

    patientsTableView.setItems(patientList);
    patientsTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    patientsTableView.refresh();
  }

}
