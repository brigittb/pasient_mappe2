package edu.ntnu.IDATT2001.brigittb.Patient.App;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Logger;

public class PatientHandler extends Application{

    /**
     * Logs errors
     */
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Sets the firstStage
     *
     * @param primaryStage first stage to be set
     * @throws IOException when fxml file is not found
     */

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader startpageLoader = new FXMLLoader(getClass().getResource("/edu/ntnu/IDATT2001/brigittb/Patient/FXML/startpage.fxml"));
        try{

           Parent root = startpageLoader.load();
           Scene scene = new Scene(root);
           primaryStage.setScene(scene);
           primaryStage.setTitle("Patient registration");
           primaryStage.show();

       }catch (IOException e){
           logger.severe("Error: An IO-exception occured. Cause: " + e.getCause());

           throw e;
       }
    }

    public static void main(String[] args) {
        launch(args);
    }

}



