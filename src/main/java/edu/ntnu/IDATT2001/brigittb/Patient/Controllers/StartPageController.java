package edu.ntnu.IDATT2001.brigittb.Patient.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * Starts teh system with a button click. Changes the scene of teh window
 */

public class StartPageController {
    @FXML private Button start;

    public void startButtonClicked() throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/edu/ntnu/IDATT2001/brigittb/Patient/FXML/frontpage.fxml")));
        Scene scene = new Scene(root);
        Stage stage = (Stage) start.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

}
