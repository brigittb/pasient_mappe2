package edu.ntnu.IDATT2001.brigittb.Patient.Model;

import edu.ntnu.IDATT2001.brigittb.Patient.Controllers.FrontPageController;

import java.util.ArrayList;


/**
 * Manages the registered Patients in the system
 */
public class PatientRegister {
    private static ArrayList <Patient> patients_register;

    public PatientRegister() {
        throw new IllegalStateException("class is non-instantiable");
    }

    public static ArrayList<Patient> getPatients_register() {
        if(patients_register == null) patients_register = new ArrayList<>();
        return patients_register;
    }

    public static void addPatientToRegister(Patient newPatient)throws Exception {
        if(PatientRegister.getPatients_register().stream().anyMatch(p -> p.getSocialSecurityNumber().equals(newPatient.getSocialSecurityNumber())))
            throw new Exception("patient already registered");
        else {
            patients_register.add(newPatient);
            FrontPageController.update();
        }
    }

    public static void deletePatientFromRegister(Patient deletePatient) throws Exception{
        if(PatientRegister.getPatients_register().stream().anyMatch(p -> p.getSocialSecurityNumber().equals(deletePatient.getSocialSecurityNumber()))) {
            patients_register.remove(deletePatient);
            FrontPageController.update();
        }else throw new Exception("patient already registered");

    }

    public static void editPatientInRegister(Patient selected_patient, String firstname, String lastname, String generalPractitioner,String social_security_number, String diagnosis){
        patients_register.set(patients_register.indexOf(selected_patient), new Patient(firstname, lastname,generalPractitioner,social_security_number,diagnosis));
    }



}
