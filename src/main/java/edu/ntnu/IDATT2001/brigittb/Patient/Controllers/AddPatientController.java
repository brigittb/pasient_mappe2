package edu.ntnu.IDATT2001.brigittb.Patient.Controllers;

import edu.ntnu.IDATT2001.brigittb.Patient.Model.Patient;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.PatientRegister;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddPatientController {

    @FXML private Button confirm, cancel;
    @FXML private TextField firstnameHolder, lastnameHolder, sosSecHolder, diagnosisHolder, gpHolder;

    public void confirm() throws Exception {
        Patient newRegisteredPatient =
                new Patient(
                        firstnameHolder.getText(),
                        lastnameHolder.getText(),
                        gpHolder.getText(),
                        sosSecHolder.getText(), diagnosisHolder.getText());
        newRegisteredPatient.setDiagnosis(diagnosisHolder.getText());

        PatientRegister.addPatientToRegister(newRegisteredPatient);
        Stage stage = (Stage) confirm.getScene().getWindow();
        stage.close();

    }

    public void cancel(){
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();

    }
}
