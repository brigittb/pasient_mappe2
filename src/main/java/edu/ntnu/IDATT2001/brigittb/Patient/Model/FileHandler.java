package edu.ntnu.IDATT2001.brigittb.Patient.Model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.*;
import java.util.Arrays;

/**
 * A class that handles read-write requests to file (Patients.csv
 */

public class FileHandler {
    /**
     * Method for importing a CVS file into the system
     *
     * @param file_path the file that is to be written to
     * @throws IOException when something wrong happens when accessing the file
     *
     */
    public static void importCSV(String file_path) throws Exception {
        FileReader fileReader = new FileReader(file_path);
        BufferedReader reader = new BufferedReader(fileReader);

        String line;
        while((line = reader.readLine()) != null) {
            String[] incoming_data = line.split(";");
            System.out.println(Arrays.toString(incoming_data));

            Patient patient = new Patient(incoming_data[0],incoming_data[1],incoming_data[2],incoming_data[3],"");
            PatientRegister.addPatientToRegister(patient);
        }
        reader.close();
    }

    /**
     * A method to export values from system to a .CSV file
     *
     * @param file_path the file path to export to
     */
    public static void exportCSV(String file_path) throws IOException {
        File file = new File(file_path);
        FileWriter w = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(w);

        for (Patient p : PatientRegister.getPatients_register()) {
            bw.write(p.getFirstName() + ";" + p.getLastName() + ";" + p.getSocialSecurityNumber() + ";" + p.getGeneralPractitioner() + ";" + p.getDiagnosis() + "\n");
        }
        bw.close();
        w.close();
    }
}
