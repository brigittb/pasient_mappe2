package edu.ntnu.IDATT2001.brigittb.Patient.Model;

public class Patient {

    private final String socialSecurityNumber;
    private String firstname;
    private String lastname;
    private String generalPractitioner;
    private String diagnosis;

    /**
     * Constructs an instance of a patient
     * @param firstname of the patient
     * @param lastname of the patient
     * @param socialSecurityNumber the patients unique social-security number
     * @param generalPractitioner the patients doctor
     * @param diagnosis the patients illness; if any
     */
    public Patient(String firstname, String lastname,String generalPractitioner,String socialSecurityNumber,
                    String diagnosis){
        this.firstname = firstname;
        this.lastname = lastname;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;

    }

    //Getters
    public String getFirstName() {
        return firstname;
    }

    public String getLastName() {
        return lastname;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getDiagnosis() {
        if(diagnosis == null ||diagnosis.equals("")) return "no illness";
        else return diagnosis;
    }


    //Setters
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }


    /**
     * ToString method.
     * @return All the information given in the constructor
     */

    public String toString() {
        return "Patient: " + firstname + " " + lastname + "\n"
                + "persnr: " + socialSecurityNumber  + "\n"
                + "fastlege: " + generalPractitioner + "\n"
                + "diagnosed with: " + getDiagnosis();
    }
}
