package edu.ntnu.IDATT2001.brigittb.Patient.Model;

        import javafx.scene.Node;
        import javafx.scene.control.*;
        import javafx.scene.image.ImageView;
        import javafx.scene.layout.AnchorPane;
        import javafx.scene.text.Text;

/**
 * Factory-class (not used)
 * @author Brigitt Bright
 *
 */

public class Factory {
    /**
     * A factory method for creating objects that inherits from node
     * @param node the requested GUI object, as a string
     * @param x The value of layoutX
     * @param y The value of layoutY
     * @return returns the object the user requested with the layout properties set by the user
     */

    public static Node getNode(String node, double x, double y) {
        if (node.equalsIgnoreCase("BUTTON")) {
            Button button = new Button();
            button.setLayoutX(x);
            button.setLayoutY(y);
            return button;
        }

        else if (node.equalsIgnoreCase("TABLEVIEW")) {
            TableView tv = new TableView<>();
            tv.setLayoutX(x);
            tv.setLayoutY(y);
            return tv;
        }
        else if (node.equalsIgnoreCase("TEXTFIELD")) {
            TextField tf = new TextField();
            tf.setLayoutX(x);
            tf.setLayoutY(y);
            return tf;
        }
        else if (node.equalsIgnoreCase("MENUBAR")) {
            MenuBar m = new MenuBar();
            m.setLayoutX(x);
            m.setLayoutY(y);
            return m;
        }
        else if (node.equalsIgnoreCase("ANCHORPANE")) {
            AnchorPane ap = new AnchorPane();
            ap.setLayoutX(x);
            ap.setLayoutY(y);
            return ap;
        }
        else if (node.equalsIgnoreCase("TEXT")) {
            Text t = new Text();
            t.setLayoutX(x);
            t.setLayoutY(y);
            return t;
        }
        else if (node.equalsIgnoreCase("IMAGEVIEW")) {
            ImageView iv = new ImageView();
            iv.setLayoutX(x);
            iv.setLayoutY(y);
            return iv;
        }
        else {
            return null;
        }
    }
}
