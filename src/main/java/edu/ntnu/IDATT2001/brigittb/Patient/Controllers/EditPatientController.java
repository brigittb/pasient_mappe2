package edu.ntnu.IDATT2001.brigittb.Patient.Controllers;

import edu.ntnu.IDATT2001.brigittb.Patient.Model.Patient;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.PatientRegister;
import edu.ntnu.IDATT2001.brigittb.Patient.Model.Selector;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * Controller for the edit_patient fxml
 */
public class EditPatientController {

    @FXML private Button edit, cancel;
    @FXML private TextField firstHolder, lastHolder, gpHolder, sosHolder, diagnosisHolder;

    Patient selectedPatientToEdit = Selector.getInstance().getPatient();

    /**
     * Initializes the textfields with the chosen patients values
     */
    public void initialize () {
        firstHolder.setText(selectedPatientToEdit.getFirstName());
        lastHolder.setText(selectedPatientToEdit.getLastName());
        sosHolder.setText(selectedPatientToEdit.getSocialSecurityNumber());
        gpHolder.setText(selectedPatientToEdit.getGeneralPractitioner());
        diagnosisHolder.setText(selectedPatientToEdit.getDiagnosis());
    }

    /**
     * Edits a chosen Patient and updates the tableView
     */
    public void edit(){
        PatientRegister.editPatientInRegister(selectedPatientToEdit,firstHolder.getText(),lastHolder.getText(),gpHolder.getText(),sosHolder.getText(),diagnosisHolder.getText());
        FrontPageController.update();
        Stage stage = (Stage) edit.getScene().getWindow();
        stage.close();
    }

    public void cancel(){
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();

    }
}
